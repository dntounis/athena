/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "HgtdClusterizationAlg.h"
#include "HGTD_ReadoutGeometry/HGTD_DetectorElement.h"

namespace ActsTrk {

  HgtdClusterizationAlg::HgtdClusterizationAlg(const std::string& name,
					       ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator)
  {}

  StatusCode HgtdClusterizationAlg::initialize()
  {
    ATH_MSG_DEBUG("Initializing " << name() << " ...");
    ATH_CHECK(detStore()->retrieve(m_hgtd_det_mgr, "HGTD"));


    ATH_CHECK(m_clusteringTool.retrieve());
    ATH_CHECK(m_rdoContainerKey.initialize());
    ATH_CHECK(m_clusterContainerKey.initialize());

    //// Initialize the clustering tool
    //ATH_CHECK(m_clusteringTool.retrieve());
    //// Initialize cluster container
    //m_clusters = new HGTDClusterContainer();

    return StatusCode::SUCCESS;

  }

  StatusCode HgtdClusterizationAlg::execute(const EventContext& ctx) const
  {
    
    SG::ReadHandle<HGTD_RDO_Container> rdoContainer(m_rdoContainerKey, ctx);
    if (!rdoContainer.isValid()) {
        ATH_MSG_ERROR("Failed to retrieve HGTD RDO container");
        return StatusCode::FAILURE;
    }

    SG::WriteHandle<xAOD::HGTDClusterContainer> clusterContainer(m_clusterContainerKey, ctx);
    ATH_CHECK(clusterContainer.record(std::make_unique<xAOD::HGTDClusterContainer>()));

    for (const auto rdoCollection : *rdoContainer) {
        if (rdoCollection->empty()) {
            continue;
        }

        Identifier rdoId = rdoCollection->identify();
        InDetDD::HGTD_DetectorElement* element = m_hgtd_det_mgr->getDetectorElement(rdoId);
        const HGTD_ID* hgtdIdHelper = element->getIdHelper();



        ATH_CHECK(m_clusteringTool->clusterize(*rdoCollection, *hgtdIdHelper, ctx, *clusterContainer));
    }


    ATH_MSG_INFO("Executing HgtdClusterizationAlg...");

    // Retrieve detector hits and convert to clusters
    //std::vector<Hit> hits = getDetectorHits();  
    //CHECK(m_clusteringTool->clusterize(hits, *m_clusters));
    // Store clusters 
    //storeClusters(m_clusters);  // Assumes storeClusters() implements this

    
    return StatusCode::SUCCESS;
  }
  
  //`void HgtdClusterizationAlg::storeClusters(HGTDClusterContainer* clusters, const EventContext& ctx) const {
  //  // Implementation to store clusters, likely interacting with other systems
  //}

}