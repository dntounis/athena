/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/HgtdClusteringTool.h"
#include "HGTD_ReadoutGeometry/HGTD_ModuleDesign.h"

namespace ActsTrk {

  HgtdClusteringTool::HgtdClusteringTool(const std::string& type,
					 const std::string& name,
					 const IInterface* parent)
    : base_class(type, name, parent)
  {}
  
  StatusCode HgtdClusteringTool::initialize()
  {
    ATH_MSG_INFO("Initializing HgtdClusteringTool...");

    ATH_CHECK(AlgTool::initialize());

    ATH_CHECK(detStore()->retrieve(m_hgtd_idHelper, "HGTD_ID"));
    ATH_CHECK(detStore()->retrieve(m_hgtd_det_mgr, "HGTD"));

    return StatusCode::SUCCESS;
  }

  StatusCode HgtdClusteringTool::clusterize(const RawDataCollection& RDOs,
				 const IDHelper& hgtdID,
				 const EventContext& ctx,
				 ClusterContainer& container) const
  {
    ATH_MSG_INFO("Clustering hits...");

    //for (const auto& RDO : RDOs) {
    //    HGTDCluster cluster = createCluster(RDO);
    //    container.addCluster(cluster);
    //}

    for (const auto* rdo : RDOs) {
        Identifier rdo_id = rdo->identify();
        InDetDD::HGTD_DetectorElement* element = m_hgtd_det_mgr->getDetectorElement(rdo_id);

        InDetDD::SiCellId si_cell_id = element->cellIdFromIdentifier(rdo_id);
        const InDetDD::HGTD_ModuleDesign& det_design = element->design();
        InDetDD::SiLocalPosition si_pos = det_design.localPositionOfCell(si_cell_id);
        Amg::Vector2D loc_pos(si_pos.xPhi(), si_pos.xEta());

        std::vector<Identifier> rdo_list = {rdo_id};
        std::vector<int> time_over_threshold = {static_cast<int>(rdo->getTOT())};

        xAOD::HGTDCluster* cluster = new xAOD::HGTDCluster();
        cluster->localPosition<3>() = loc_pos;
        //cluster->setHashAndIndex(container.identifyHash(), container.size());
        cluster->setRDOlist(std::move(rdo_list));
        cluster->setToTlist(std::move(time_over_threshold));

        container.push_back(cluster);
    }


    return StatusCode::SUCCESS;
  }

//HGTDCluster HgtdClusteringTool::createCluster(const Hit& hit) {
//    HGTDCluster cluster;
//    // Fill cluster data based on hit properties
//    cluster.setCoordinates(hit.x(), hit.y());
//    cluster.setTime(hit.time());
//    // Additional fields as necessary
//    return cluster;
//}


} // namespace

