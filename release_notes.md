This sweep contains the following MRs:
 * !75931 Updates to LF monitoring for HI, temporary removed AFP items ~Trigger, ~TriggerMenu
 * !75917 LArFCalTowerBuilderTool : improve thread safety of look-up table building ~Calorimeter, ~LAr
 * !75837 Improve protection for cells container in TrigTowerMaker and avoid double container CaloCells in HI menu ~Egamma, ~Trigger, ~TriggerMenu
 * !75889 HltEventLoopMgr: fix race condition during stop ~Trigger
 * !75911 TriggerMenuMT: fix flake8 warnings seen when updating to LCG_106a ~Trigger, ~TriggerMenu
 * !75910 Fixing Flake8 warnings in AnalysisAlgorithmsConfig seen when updating to LCG106a ~Analysis, ~CPAlgorithms
 * !75915 HISTPostProcess_tf.py: add trfArgClass for custom arguments ~Reconstruction
 * !75909 Updates to PyDumpers to support LCG_106a ~EDM
 * !75894 Set conditionsTag for test_zprime_tide.sh ~InnerDetector, ~PhysValidation
 * !75874 MuonCabling - Backporting of the cabling into 24.0 ~MuonSpectrometer, ~sweep:ignore
 * !75815 fix alignmon folders in DQ HI config ~DQ
 * !75074 LAr HVlines  noise flag ~Calorimeter, ~DQ, ~LAr, ~Run2-DataReco-output-changed, ~Run2-MCReco-output-changed, ~Run3-DataReco-output-changed, ~Run3-MCReco-output-changed, ~Tools, ~Trigger
 * !75913 Updates to CaloRec package to support LCG_106a ~Calorimeter
 * !75914 Updates to DQDefects package to support LCG_106a ~DQ
 * !75884 Add new LAr patching ~LAr
 * !75862 AthenaCommon: fix flake8 warnings seen after update to LCG106a ~Core, ~sweep:ignore
 * !75847 AthContainers: Add warnUnlocked arguments. ~Core
 * !75865 Fixing Flake8 warnings in multiple packages seen when updating to LCG106a ~Analysis, ~Database, ~Egamma, ~Generators, ~Overlay, ~Reconstruction, ~Simulation, ~Tools, ~sweep:ignore
 * !75844 integration of FADC correction into ZDC reconstruction ~ForwardDetectors
 * !75866 Fixing Flake8 warnings in MVAUtils seen when updating to LCG106a ~Reconstruction
 * !75871 Updating T0 Trigger reference to run 488991 (ATR-30503) ~DQ, ~Trigger
 * !75839 Update L1CaloCondConfig.py - incorrect folder being used for loading strategy ... removed unused folder ~L1Calo, ~Trigger
 * !75864 ATR-29028: Lower DPHI-eTAU1 thresholds to 0.8, 0.8 ~Trigger, ~TriggerMenu
 * !75838 Allow the possibility of weighting energy-deposits in LArSimHits made by Frozen Showers ~LAr, ~Simulation
 * !75876 Introduce check against  fixed reference for IDPVM simreco ART tests (ATLIDTRKCP-646) ~InnerDetector, ~PhysValidation
 * !75867 Fixing Flake8 warnings in PyDumper seen when updating to LCG106a ~EDM, ~sweep:ignore
 * !75831 RootUtils: fix build with LCG_106a ~Core
 * !75835 Updates to Rivet_i and TruthRivetUtils to support the version of Rivet in LCG_106a ~Generators
 * !75846 AthenaServices+RootStorageSvc+RootAuxDynIO: Fix compilation errors in 24.0 + LCG106 ~Core, ~Database, ~sweep:ignore
 * !75860 Projects: disable Boost version warning ~Build, ~Simulation
 * !75825 TRTCalib - Adding TRTCalib_StrawStatusPlots in replace of TRTCalib_StrawStatusReport.C root macro ~InnerDetector
 * !75830 CaloRecGPU: copy version from main branch ~CUDA, ~Calorimeter
 * !75623 TrigServices: disable handling of Ctrl-C in CoreDumpSvc ~Trigger
 * !75829 TrigInDetCUDA: fix build with LCG_106a ~CUDA, ~ITk, ~Trigger
 * !75826 TrigConfMuctpi: Fix compilation with boost 1.85. ~Trigger
 * !74348 Updating the global monitoring script to be used for Online/ART ~Reconstruction
 * !75782 Auxdata replaced with ConstAccessors in ZdcNtuple ~ForwardDetectors
 * !75813 Align IDPVM Z' TIDE ART test with other sim+reco tests ~InnerDetector, ~PhysValidation
 * !75812 Updated IDPVM ART mu100 GeV references to 24.0.71 ~InnerDetector, ~PhysValidation
 * !75797 Campaigns: Add MC23aNoPileUp, MC23dNoPileUp, MC23eNoPileUp configurations ~Tools
